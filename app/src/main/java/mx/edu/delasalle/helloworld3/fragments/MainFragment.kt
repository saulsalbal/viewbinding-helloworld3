package mx.edu.delasalle.helloworld3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.RenderProcessGoneDetail
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import mx.edu.delasalle.helloworld3.R


class MainFragment : Fragment(R.layout.fragment_main) {

    private var _binding:MainFragment? = null
    private val binding get() = _binding!!
    private lateinit var btnFragment2: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        _binding = MainFragment.inflate(inflater, container, false)
        return binding.view

    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnFragment2 = binding.btnFragment2

        btnFragment2.setOnClickListener {

        var nombre = "Hugo"
           /*findNavController().navigate(R.id.action_mainFragment_to_detailFragment, bundleOf(
               "nombre" to nombre
           ))*/

            val action = MainFragmentDirections.actionMainFragmentToDetailFragment()
            action.nombre = nombre

            findNavController().navigate(action)
        }
    }
}