package mx.edu.delasalle.helloworld3.activities

import android.app.Activity
import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import mx.edu.delasalle.helloworld3.R
import mx.edu.delasalle.helloworld3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    private lateinit var editName:EditText

    private lateinit var binding: ActivityMainBinding

    private val TAG = "TEST"
    private val TAG2 = "LifeCycle"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)



        binding.btnSayHello.setOnClickListener{
            val name = editName.text.toString()
            sayHello(name)
        }

        binding.btnSayHello.setOnClickListener{
            val name = editName.text.toString()
            var intent = Intent(this,HelloActivity::class.java)
            intent.putExtra("name",name)
            //startActivity(intent)

            startActivityForResult(intent,1)
        }

        binding.btnToolbar.setOnClickListener{

            var intent = Intent(this,ToolbarActivity::class.java)

            startActivity(intent)

        }

        binding.btnFragments.setOnClickListener{

            var intent = Intent(this, FragmentsActivity::class.java)

            startActivity(intent)

        }

        binding.btnFragments2.setOnClickListener{

        }

        Log.e(TAG,"This is an error")
        Log.d(TAG,"This is for debugging")
        Log.w(TAG,"This is a warning")
        Log.i(TAG,"This is informative")

        Log.d(TAG2,"onCreate")


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 1 && resultCode == Activity.RESULT_OK)
        {
            val valor1= data?.getStringExtra("valor1")

            Toast.makeText(this,"$valor1",Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart(){
        super.onStart()
        Log.d("LifeCycle", "onStart")
    }

    override fun onResume(){
        super.onResume()
        Log.d("LifeCycle", "onResume")
    }

    override fun onPause(){
        super.onPause()
        Log.d("LifeCycle", "onPause")
    }

    override fun onStop(){
        super.onStop()
        Log.d("LifeCycle", "onStop")
    }

    override fun onRestart(){
        super.onRestart()
        Log.d("LifeCycle", "onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("LifeCycle", "onDestroy")
    }



    fun sayHello(name:String){
        var hello = getString(R.string.hello)

        binding.txtLabel.text = "$hello"
    }
}