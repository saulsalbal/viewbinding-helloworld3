package mx.edu.delasalle.helloworld3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import mx.edu.delasalle.helloworld3.R
import androidx.fragment.app.commit


class FirstFragment : Fragment(R.layout.fragment_first) {

    private var _binding:FirstFragment? = null
    private val binding get() = _binding!!

    private lateinit var btnFragment2: Button
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        _binding = FirstFragment.inflate(inflater, container, false)
        return binding.view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnFragment2 = binding.btnFragment2

        btnFragment2.setOnClickListener {
            requireActivity().supportFragmentManager.commit {
                replace(R.id.fragment_container_view,SecondFragment())
            }
        }

    }
}




