package mx.edu.delasalle.helloworld3.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import mx.edu.delasalle.helloworld3.R
import mx.edu.delasalle.helloworld3.databinding.ActivityHelloBinding

class HelloActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHelloBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val name = intent.getStringExtra("name")
        val hello = getString(R.string.hello)
        binding.txtName.text = "$hello $name"

        binding.btnBack.setOnClickListener {

            //var intent = Intent(this,MainActivity::class.java)

            //startActivity(intent)
            finish()

        }

        binding.btnBackReturn.setOnClickListener {

            var intent = Intent()

            intent.putExtra("valor1", "Test1")

            setResult(Activity.RESULT_OK,intent)

            finish()

        }

    }
}