package mx.edu.delasalle.helloworld3.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
//import android.widget.Toolbar
import mx.edu.delasalle.helloworld3.R
import androidx.appcompat.widget.Toolbar
import mx.edu.delasalle.helloworld3.databinding.ActivityToolbarBinding

class ToolbarActivity : AppCompatActivity() {

    private lateinit var binding: ActivityToolbarBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toolbar)
        binding = ActivityToolbarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = binding.toolbar

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.mymenu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId)
        {
            android.R.id.home -> finish()
            R.id.item_1 -> Toast.makeText(this, "Element 1", Toast.LENGTH_LONG).show()
            R.id.item_2 -> Toast.makeText(this, "Element 2", Toast.LENGTH_LONG).show()
        }

        return super.onOptionsItemSelected(item)
    }
}